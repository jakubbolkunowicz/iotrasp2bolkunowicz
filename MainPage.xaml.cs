/********************************************************************************************************************************
 *
 *                                              EMBEDDED SYSTEMS LABORATORY 2016
 *
 *  Sample LED control program for students laboratory purposes. 
 *  Presents the Raspberry Pi 2/3 IoT ability to control
 *  GPIO output pins blinking LEDs.
 *
 *
 *        Raspberry Pi GPIO Layout - Model B+
 *  1 - 3V3     Power                      2 - 5V Power
 *  3 - GPIO2   SDA1 I2C                   4 - 5V Power
 *  5 - GPIO3   SCL1 I2C                   6 - GND
 *  7 - GPIO4                              8 - GPIO14   UART0_TXD
 *  9 - GND                               10 - GPIO15   UART0_RXD
 * 11 - GPIO17                            12 - GPIO18   PCM_CLK
 * 13 - GPIO27                            14 - GND
 * 15 - GPIO22                            16 - GPIO23
 * 17 - 3V3     Power                     18 - GPIO24
 * 19 - GPIO10  SPI0_MOSI                 20 - GND
 * 21 - GPIO9   SPI0_SCLK                 22 - GPIO25
 * 23 - GPIO11                            24 - GPIO8    SPI0_CE0_N
 * 25 - GND                               26 - GPIO7    SPI0_CE1_N 
 * 27 - ID_SD   I2C ID EEPROM             28 - ID_SC    I2C ID EEPROM
 * 29 - GPIO5                             30 - GND
 * 31 - GPIO6                             32 - GPIO12
 * 33 - GPIO13                            34 - GND
 * 35 - GPIO19                            36 - GPIO16
 * 37 - GPIO26                            38 - GPIO20
 * 39 - GND                               40 - GPIO21
 *
 ********************************************************************************************************************************/

using System;
using Windows.Devices.Gpio;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace IoTGPIO {
  public sealed partial class MainPage : Page {
    // Raspberry Pi GPIO pins used for LEDs coontrol 
    private const int LED_PIN1 = 4;   // GPIO04
    private const int LED_PIN2 = 23;  // GPIO23
    private const int LED_PIN3 = 24;  // GPIO24
    private const int LED_PIN4 = 12;  // GPIO12

    private GpioPin pin1;
    private GpioPin pin2;
    private GpioPin pin3;
    private GpioPin pin4;

    // Raspberry Pi GPIO Pin variables for blinking effect
    private GpioPinValue[] pinValue1 = new GpioPinValue[16];
    private GpioPinValue[] pinValue2 = new GpioPinValue[16];
    private GpioPinValue[] pinValue3 = new GpioPinValue[16];
    private GpioPinValue[] pinValue4 = new GpioPinValue[16];

    ThreadPoolTimer gTimer;


    private SolidColorBrush redBrush = new SolidColorBrush(Windows.UI.Colors.Red);
    private SolidColorBrush grayBrush = new SolidColorBrush(Windows.UI.Colors.LightGray);
    private int Counter = 0;
    //dodano zmienne pomocnicze
    private bool licz = false; // Zmienna pomocnicza jako wylacznik licznika
    private bool aside = true; // Zmienna pomocnicza sluzaca do wyznaczania kierunku liczenia

    public MainPage() {
      InitializeComponent();                                // Initialize XAML Display view
      Unloaded += MainPage_Unloaded;

      InitLEDs();                                           // Initialize LEDs blinking procedure
      InitGPIO();                                           // Initialze GPIO pins

            gTimer = ThreadPoolTimer.CreatePeriodicTimer(GPIOTimer, TimeSpan.FromSeconds(1));

            //Dodano obsluge przyciskow
            count.Checked += count_check; // Jesli checkbox "Licz" zostal zaznaczony
            count.Unchecked += count_uncheck; // Jesli checkbox "Licz" zostal odznaczony. Domyslnie odznaczony

            increment.Checked += increment_check; // Jesli zaznaczono radiobutton "Inkrementuj". Domyslnie zaznaczony
            decrement.Checked += decrement_check; // Jesli zaznaczono radiobutton "Dekrementuj".

            PLUS.Click += PLUS_Click; // Jesli wcisnieto przycisk "+"
            MINUS.Click += MINUS_Click; // Jesli wcisnieto przycisk "-"

        } // End of MainPage

        // Funkcja pomocnicza sluzaca do aktualizacji stanu LED
        private void updateLedState() 
        {
            LED1.Fill = pinValue1[Counter] == GpioPinValue.High ? redBrush : grayBrush;   // Draw LED1 elipse Red/Gray = High/Low
            LED2.Fill = pinValue2[Counter] == GpioPinValue.High ? redBrush : grayBrush;   // Draw LED2 elipse Red/Gray = High/Low
            LED3.Fill = pinValue3[Counter] == GpioPinValue.High ? redBrush : grayBrush;   // Draw LED3 elipse Red/Gray = High/Low
            LED4.Fill = pinValue4[Counter] == GpioPinValue.High ? redBrush : grayBrush;   // Draw LED4 elipse Red/Gray = High/Low
            //pin1.Write(pinValue1[Counter]);  
            // Jezeli pracowalibysmy na realnym sprzecie, 
            // wowczas pod kazda aktualizacja stanu LED trzeba dopisac aktualizacje pinow
        }

        // Jesli zaznaczono "Dekrementuj" zmienna aside = false;
        private void decrement_check(object sender, RoutedEventArgs e) 
        {
            aside = false;
        }

        // Jesli zaznaczono "Dekrementuj" zmienna aside = true;
        private void increment_check(object sender, RoutedEventArgs e) 
        {
            aside = true;
        }

        // Jesli zaznaczono "Licz" zmienna licz = true;
        private void count_check(object sender, RoutedEventArgs e) 
        {
            licz = true;
        }

        // Jesli nie zaznaczono "Licz" zmienna licz = false;
        private void count_uncheck(object sender, RoutedEventArgs e) 
        {
            licz = false;
        }

        // Jesli kliknieto "+" dodaj zawartosc Counter i zaktualizuj stan LEDow;
        private void PLUS_Click(object sender, RoutedEventArgs e) 
        {
            Counter++;
            if (Counter > 15) Counter = 0;
            updateLedState();
        }

        // Jesli kliknieto "-" odejmij zawartosc Counter i zaktualizuj stan LEDow;
        private void MINUS_Click(object sender, RoutedEventArgs e) 
        {
            Counter--;
            if (Counter < 0) Counter = 15;
            updateLedState();
        }

        private void MainPage_Unloaded(object sender, object args)  {
      // Cleanup
      pin1.Dispose();
      pin2.Dispose();
      pin3.Dispose();
      pin4.Dispose();
    } // End of MainPage_Unloaded


    // Initial LEDs state 8 stages
    void InitLEDs() {
      pinValue1[0] = GpioPinValue.Low;                     // pins value state 0
      pinValue2[0] = GpioPinValue.Low;
      pinValue3[0] = GpioPinValue.Low;
      pinValue4[0] = GpioPinValue.Low;

      pinValue1[1] = GpioPinValue.Low;                     // pins value state 1
      pinValue2[1] = GpioPinValue.Low;
      pinValue3[1] = GpioPinValue.Low;
      pinValue4[1] = GpioPinValue.High;

      pinValue1[2] = GpioPinValue.Low;                     // pins value state 2
      pinValue2[2] = GpioPinValue.Low;
      pinValue3[2] = GpioPinValue.High;
      pinValue4[2] = GpioPinValue.Low;

      pinValue1[3] = GpioPinValue.Low;                     // pins value state 3
      pinValue2[3] = GpioPinValue.Low;
      pinValue3[3] = GpioPinValue.High;
      pinValue4[3] = GpioPinValue.High;

      pinValue1[4] = GpioPinValue.Low;                      // pins value state 4
      pinValue2[4] = GpioPinValue.High;
      pinValue3[4] = GpioPinValue.Low;
      pinValue4[4] = GpioPinValue.Low;

      pinValue1[5] = GpioPinValue.Low;                      // pins value state 5
      pinValue2[5] = GpioPinValue.High;
      pinValue3[5] = GpioPinValue.Low;
      pinValue4[5] = GpioPinValue.High;

      pinValue1[6] = GpioPinValue.Low;                      // pins value state 6
      pinValue2[6] = GpioPinValue.High;
      pinValue3[6] = GpioPinValue.High;
      pinValue4[6] = GpioPinValue.Low;

      pinValue1[7] = GpioPinValue.Low;                      // pins value state 7
      pinValue2[7] = GpioPinValue.High;
      pinValue3[7] = GpioPinValue.High;
      pinValue4[7] = GpioPinValue.High;

            pinValue1[8] = GpioPinValue.High;                     // pins value state 0
            pinValue2[8] = GpioPinValue.Low;
            pinValue3[8] = GpioPinValue.Low;
            pinValue4[8] = GpioPinValue.Low;

            pinValue1[9] = GpioPinValue.High;                     // pins value state 1
            pinValue2[9] = GpioPinValue.Low;
            pinValue3[9] = GpioPinValue.Low;
            pinValue4[9] = GpioPinValue.High;

            pinValue1[10] = GpioPinValue.High;                     // pins value state 2
            pinValue2[10] = GpioPinValue.Low;
            pinValue3[10] = GpioPinValue.High;
            pinValue4[10] = GpioPinValue.Low;

            pinValue1[11] = GpioPinValue.High;                     // pins value state 3
            pinValue2[11] = GpioPinValue.Low;
            pinValue3[11] = GpioPinValue.High;
            pinValue4[11] = GpioPinValue.High;

            pinValue1[12] = GpioPinValue.High;                      // pins value state 4
            pinValue2[12] = GpioPinValue.High;
            pinValue3[12] = GpioPinValue.Low;
            pinValue4[12] = GpioPinValue.Low;

            pinValue1[13] = GpioPinValue.High;                      // pins value state 5
            pinValue2[13] = GpioPinValue.High;
            pinValue3[13] = GpioPinValue.Low;
            pinValue4[13] = GpioPinValue.High;

            pinValue1[14] = GpioPinValue.High;                      // pins value state 6
            pinValue2[14] = GpioPinValue.High;
            pinValue3[14] = GpioPinValue.High;
            pinValue4[14] = GpioPinValue.Low;

            pinValue1[15] = GpioPinValue.High;                      // pins value state 7
            pinValue2[15] = GpioPinValue.High;
            pinValue3[15] = GpioPinValue.High;
            pinValue4[15] = GpioPinValue.High;
        }

    // Initialize device GPIO. All LEDs control pins set to the output state
#if X86
    private void InitGPIO() {

      Time.Text = "App initialized.";
    }
#else
    private void InitGPIO() {
      var GPIO = GpioController.GetDefault();

      // Show an error if there is no GPIO controller
      if (GPIO == null)  {
        pin1 = null;
        pin2 = null;
        pin3 = null;
        pin4 = null;

        Time.Text = "There is no GPIO controller on this device.";
        return;
      }
      pin1 = GPIO.OpenPin(LED_PIN1);
      pin2 = GPIO.OpenPin(LED_PIN2);
      pin3 = GPIO.OpenPin(LED_PIN3);
      pin4 = GPIO.OpenPin(LED_PIN4);

      pin1.Write(pinValue1[0]);                     // set pin1 state LOW/HIGH
      pin1.SetDriveMode(GpioPinDriveMode.Output);
      pin2.Write(pinValue2[0]);                     // set pin2 state LOW/HIGH
      pin2.SetDriveMode(GpioPinDriveMode.Output);
      pin3.Write(pinValue3[0]);                     // set pin3 state LOW/HIGH
      pin3.SetDriveMode(GpioPinDriveMode.Output);
      pin4.Write(pinValue4[0]);                     // set pin4 state LOW/HIGH
      pin4.SetDriveMode(GpioPinDriveMode.Output);

      Time.Text = "GPIO pins initialized correctly.";
    }
#endif

#if X86
    // XAML Timer callback function set to be executed each 500 ms
    private async void GPIOTimer(ThreadPoolTimer timer) {
            // Jesli licz = true wowczas licznik dziala z interwalem czasowym 1 sekunda
            if (licz == true) 
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    // W zaleznosci od stanu zmiennej aside, licznik inkrementuje badz dekrementuje
                    if (aside == true) Counter++; 
                    else Counter--;
                    // Przepelnienie Counter powoduje powrot na poczatek/koniec tablicy
                    if (Counter > 15) Counter = 0; 
                    if (Counter < 0) Counter = 15;
                    updateLedState();
                }).AsTask();
            }
    }
#else
    // XAML Timer callback function set to be executed each 500 ms
    private async void GPIOTimer(ThreadPoolTimer timer) {
      await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => {
        LED1.Fill = pinValue1[Counter] == GpioPinValue.High ? redBrush : grayBrush;   // Draw LED1 elipse Red/Gray = High/Low
        pin1.Write(pinValue1[Counter]);                                               // Set pin1 value High/Low
        LED2.Fill = pinValue2[Counter] == GpioPinValue.High ? redBrush : grayBrush;   // Draw LED2 elipse Red/Gray = High/Low
        pin2.Write(pinValue2[Counter]);                                               // Set pin2 value High/Low
        LED3.Fill = pinValue3[Counter] == GpioPinValue.High ? redBrush : grayBrush;   // Draw LED3 elipse Red/Gray = High/Low
        pin3.Write(pinValue3[Counter]);                                               // Set pin3 value High/Low
        LED4.Fill = pinValue4[Counter] == GpioPinValue.High ? redBrush : grayBrush;   // Draw LED4 elipse Red/Gray = High/Low
        pin4.Write(pinValue4[Counter]);                                               // Set pin4 value High/Low
        Counter++;
        if (Counter > 7)
          Counter = 0;
      }).AsTask();
    }
#endif
  }
}
